# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

WX_GTK_VER="3.0"
inherit cmake desktop fcaps flag-o-matic wxwidgets


DESCRIPTION="A PC emulator that specializes in running old operating systems and software"
HOMEPAGE="
	https://pcem-emulator.co.uk/
	https://github.com/sarah-walker-pcem/pcem/
"
if [[ ${PV} = *9999* ]]; then
	inherit git-r3
	ROMDIR="${WORKDIR}/roms"
else
	ROM_REVISION="75bd118dd86378cac1d9d55e29e26ef82d6d57ef"
	ROMDIR="${WORKDIR}/PCem-ROMs-${ROM_REVISION}"
	SRC_URI="
		https://pcem-emulator.co.uk/files/PCemV${PV}Linux.tar.gz
		https://github.com/BaRRaKudaRain/PCem-ROMs/archive/${ROM_REVISION}.tar.gz -> PCem-ROMs-${ROM_REVISION}.tar.gz
	"
fi

S="${WORKDIR}/${P}"

LICENSE="GPL-2+"
SLOT="0"
KEYWORDS="amd64"
IUSE="alsa networking pcap"
REQUIRED_USE="pcap? ( networking )"

RDEPEND="
	alsa? ( media-libs/alsa-lib )
	media-libs/libsdl2
	media-libs/openal
	x11-libs/wxGTK:${WX_GTK_VER}[tiff,X]
	pcap? ( net-libs/libpcap )
"
DEPEND="${RDEPEND}"
BDEPEND="virtual/pkgconfig"

DOCS=( "README.md" "TESTED.md" )

src_fetch() {
	if [[ ${PV} = *9999* ]]; then
		git-r3_fetch https://github.com/sarah-walker-pcem/pcem
		git-r3_fetch https://github.com/BaRRaKudaRain/PCem-ROMs
	fi
}

src_unpack() {
	if [[ ${PV} = *9999* ]]; then
		src_fetch
		git-r3_checkout https://github.com/sarah-walker-pcem/pcem
		git-r3_checkout https://github.com/BaRRaKudaRain/PCem-ROMs "${ROMDIR}"
	else
		default
	fi
}

src_configure() {
	setup-wxwidgets

	# pcem is a buggy piece of shit
	append-cflags -mstackrealign -fno-strict-aliasing

	# Does not compile with -fno-common.
	# See https://pcem-emulator.co.uk/phpBB3/viewtopic.php?f=3&t=3443
	append-cflags -fcommon

	# LTO needs to be filtered
	# See https://bugs.gentoo.org/854528
	filter-lto
	append-flags -fno-strict-aliasing

	local mycmakeargs=(
		-DPCEM_LIB_DIR=$(get_libdir)
		-DPCEM_DISPLAY_ENGINE=wxWidgets # Qt Mode is not yet implemented
		-DUSE_NETWORKING=$(usex networking ON OFF)
		-DUSE_PCAP_NETWORKING=$(usex pcap ON OFF)
		-DUSE_ALSA=$(usex alsa ON OFF)
	)
	cmake_src_configure
}

src_install() {
	cmake_src_install

	newicon src/wx-ui/icons/16x16/motherboard.png pcem.png
	make_desktop_entry "pcem" "PCem" pcem "Development;Utility"

	einstalldocs

	insinto /usr/share/pcem/roms
	doins -r "${ROMDIR}/"*
}

pkg_postinst() {
	if use pcap; then
		chgrp pcap "${EROOT}"/usr/bin/pcem
		# fcaps cap_dac_read_search,cap_net_raw,cap_net_admin "${EROOT}"/usr/bin/pcem
		fcaps -m 711 -M 710 cap_net_raw,cap_net_admin "${EROOT}"/usr/bin/pcem
	fi
}
