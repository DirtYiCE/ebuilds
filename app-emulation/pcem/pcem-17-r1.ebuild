# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

WX_GTK_VER="3.0"
inherit autotools desktop flag-o-matic wxwidgets

ROM_REVISION="75bd118dd86378cac1d9d55e29e26ef82d6d57ef"

DESCRIPTION="A PC emulator that specializes in running old operating systems and software"
HOMEPAGE="
	https://pcem-emulator.co.uk/
	https://github.com/sarah-walker-pcem/pcem/
"
SRC_URI="
	https://pcem-emulator.co.uk/files/PCemV${PV}Linux.tar.gz
	https://github.com/BaRRaKudaRain/PCem-ROMs/archive/${ROM_REVISION}.tar.gz -> PCem-ROMs-${ROM_REVISION}.tar.gz
"
S="${WORKDIR}"

LICENSE="GPL-2+"
SLOT="0"
KEYWORDS="amd64"
IUSE="alsa networking"

RDEPEND="
	alsa? ( media-libs/alsa-lib )
	media-libs/libsdl2
	media-libs/openal
	x11-libs/wxGTK:${WX_GTK_VER}[tiff,X]
"
DEPEND="${RDEPEND}"
BDEPEND="virtual/pkgconfig"

DOCS=( "README.md" "TESTED.md" )

PATCHES=( "${FILESDIR}/${PN}-17-respect-cflags.patch" )

src_prepare() {
	default

	eautoreconf
}

src_configure() {
	setup-wxwidgets

	# Does not compile with -fno-common.
	# See https://pcem-emulator.co.uk/phpBB3/viewtopic.php?f=3&t=3443
	append-cflags -fcommon

	# LTO needs to be filtered
	# See https://bugs.gentoo.org/854528
	filter-lto
	append-flags -fno-strict-aliasing

	local myeconfargs=(
		--enable-release-build
		$(use_enable alsa)
		$(use_enable networking)
		--with-wx-config="${WX_CONFIG}"
	)

	econf "${myeconfargs[@]}"
}

src_install() {
	default

	insinto /usr/share/pcem
	doins -r configs nvr roms

	newicon src/icons/32x32/motherboard.png pcem.png
	make_desktop_entry "pcem" "PCem" pcem "Development;Utility"

	einstalldocs

	insinto /usr/share/pcem/roms
	doins -r "${WORKDIR}/PCem-ROMs-${ROM_REVISION}/"*
}
