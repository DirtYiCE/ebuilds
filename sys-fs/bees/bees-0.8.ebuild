# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit linux-info systemd toolchain-funcs

DESCRIPTION="Best-Effort Extent-Same, a btrfs dedup agent"
HOMEPAGE="https://github.com/Zygo/bees"

if [[ ${PV} == 9999 ]] ; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/Zygo/bees.git"
else
	SRC_URI="https://github.com/Zygo/bees/archive/v${PV}.tar.gz -> ${P}.tar.gz"
	KEYWORDS="~amd64 ~arm64"
fi

LICENSE="GPL-3"
SLOT="0"
IUSE="tools"

DEPEND="
	>=sys-apps/util-linux-2.30.2
	>=sys-fs/btrfs-progs-4.20.2
"
RDEPEND="${DEPEND}"

PATCHES=("${FILESDIR}/bees-0.7.2-beesd-no-uuid.patch")

CONFIG_CHECK="~BTRFS_FS"
ERROR_BTRFS_FS="CONFIG_BTRFS_FS: bees does currently only work with btrfs"

pkg_pretend() {
	if [[ ${MERGE_TYPE} != buildonly ]]; then
		if kernel_is -gt 5 1 0 && kernel_is -lt 5 4 14; then
			ewarn "Kernel 5.1, 5.2, and 5.3 should not be used with btrfs due to a severe regression"
			ewarn "that can lead to fatal metadata corruption. This issue is fixed in kernel 5.4.14 and later."
			ewarn "https://github.com/Zygo/bees/blob/master/docs/btrfs-kernel.md"
			ewarn
		fi

		elog "Bees recommends running the latest current kernel for performance and"
		elog "reliability reasons, see README.md."
	fi
}

src_prepare() {
	default
	sed -i 's/ -Werror//' makeflags || die
}

src_configure() {
	tc-export CC CXX AR
	cat >localconf <<-EOF || die
		ETC_PREFIX="${EPREFIX}/etc"
		LIBEXEC_PREFIX="${EPREFIX}/usr/libexec"
		PREFIX="${EPREFIX}/usr"
		SYSTEMD_SYSTEM_UNIT_DIR="$(systemd_get_systemunitdir)"
		DEFAULT_MAKE_TARGET=all
	EOF
	if [[ ${PV} != "9999" ]] ; then
		echo BEES_VERSION=v${PV} >>localconf || die
	fi
	if use tools; then
		echo OPTIONAL_INSTALL_TARGETS=install_tools >>localconf || die
	fi
}

src_compile() {
	default
	# localconf quotes leak in the systemd unit but are still needed for spaces
	sed -i 's/"//g' scripts/beesd@.service || die
}

src_install() {
	default
	newinitd "${FILESDIR}"/bees.initd bees
}
