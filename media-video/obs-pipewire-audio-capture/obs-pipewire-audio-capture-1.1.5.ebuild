# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit xdg cmake

if [[ ${PV} == 9999 ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/dimtpap/obs-pipewire-audio-capture"
else
	SRC_URI="https://github.com/dimtpap/obs-pipewire-audio-capture/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz"
	KEYWORDS="~amd64"
fi

DESCRIPTION="OBS PipeWire audio capture"
HOMEPAGE="https://github.com/dimtpap/obs-pipewire-audio-capture"

LICENSE="GPL-2"
SLOT="0"

COMMON_DEPEND="
	media-video/obs-studio
	media-video/pipewire
"
DEPEND="${COMMON_DEPEND}"
RDEPEND="${COMMON_DEPEND}"

src_unpack() {
	default

	if [[ ${PV} == 9999 ]]; then
		git-r3_src_unpack
	fi
}
