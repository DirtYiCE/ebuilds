# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2
# based on mva and zGentoo overlay

EAPI=8

inherit unpacker

# GENVENDOR=y

DESCRIPTION="The open-source platform for monitoring and observability."
HOMEPAGE="http://grafana.org"
EGO_PN="github.com/${PN}/${PN}"
SRC_URI="https://${EGO_PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"
[[ -n "${GENVENDOR}" ]] || SRC_URI+=" https://home.dirty-ice.org/packages/${PN}-vendor-${PV}.tar.zst"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="+cli server"
REQUIRED_USE="|| ( cli server )"

DEPEND="
	server? (
		acct-group/grafana
		acct-user/grafana
	)
	!www-apps/grafana-bin
"

BDEPEND="
	app-arch/zstd
	server? (
		net-libs/nodejs[icu]
		sys-apps/yarn
	)
	>=dev-lang/go-1.15
"

[[ -n "${GENVENDOR}" ]] && RESTRICT="network-sandbox"

src_prepare() {
	default

	if use server; then
		sed -i -e 's/^;reporting_enabled = .*/reporting_enabled = false/' \
			-e 's/^;check_for_updates = .*/check_for_updates = false/' \
			-e 's|^;data = .*|data = /var/lib/grafana|' \
			-e 's|^;logs = .*|logs = /var/log/grafana|' \
			-e 's|^;plugins = .*|plugins = /var/lib/grafana/plugins|' \
			conf/sample.ini || die

		yarn config set yarn-offline-mirror "${WORKDIR}/yarn_vendor"
		export npm_config_build_from_source=true
		# don't use the network FFS
		export CYPRESS_INSTALL_BINARY=0
		yarn install $([[ -n "${GENVENDOR}" ]] || echo --offline) || die
	fi
}

src_compile() {
	export GOMODCACHE="${WORKDIR}/go-mod"
	use cli && emake build-cli
	use server && emake build-server build-js

	if ! [[ -z "${GENVENDOR}" ]]; then
		cd "${WORKDIR}"
		einfo "Generating vendor pkg"
		tar c go-mod yarn_vendor | zstd -v -T0 -19 > ${PN}-vendor-${PV}.tar.zst
		die "vendor pkg created in ${WORKDIR}"
	fi
}

src_install() {
	use cli && dobin bin/*/grafana-cli

	if use server; then
		insinto /etc/grafana
		newins conf/sample.ini grafana.ini
		fowners -R grafana:grafana /etc/grafana
		fperms 0750 /etc/grafana

		insinto /usr/share/grafana
		doins -r conf public
		insinto /usr/share/grafana/plugins-bundled/internal/input-datasource
		doins -r plugins-bundled/internal/input-datasource/dist/*

		dobin bin/*/grafana-server

		newinitd "${FILESDIR}/grafana.initd" grafana

		keepdir /var/log/grafana
		fowners grafana:grafana /var/log/grafana
		fperms 0750 /var/log/grafana

		keepdir /var/lib/grafana
		fowners grafana:grafana /var/lib/grafana
		fperms 0750 /var/lib/grafana
	fi
}
