EAPI=8

DESCRIPTION="simple randr daemon"
HOMEPAGE="https://github.com/jceb/srandrd"
SRC_URI="https://github.com/jceb/srandrd/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="
	x11-libs/libX11
	x11-libs/libXrandr
	x11-libs/libXinerama
"
RDEPEND="${DEPEND}"
BDEPEND=""

PATCHES=("${FILESDIR}/srandrd-0.6.3-daemonize.patch")
