# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
inherit linux-mod-r1

DESCRIPTION="Kernel modules for some laptops sold by Laptop With Linux"
HOMEPAGE="https://github.com/comexr/lwl-drivers"

SRC_URI="https://github.com/comexr/lwl-drivers/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-2"
KEYWORDS="~amd64 ~x86"
SLOT="0"

CONFIG_CHECK="INPUT_SPARSEKMAP"

src_compile(){
	local modargs=( KDIR="${KV_OUT_DIR}" )
	local modlist=(
		ite_8291=::src/ite_8291
		ite_8291_lb=::src/ite_8291_lb
		ite_8297=::src/ite_8297
		ite_829x=::src/ite_829x
		lwl_compatibility_check=::src/lwl_compatibility_check
		lwl_io=::src/lwl_io
		lwl_nb02_nvidia_power_ctrl=::src/lwl_nb02_nvidia_power_ctrl
		lwl_nb04_keyboard=::src/lwl_nb04
		lwl_nb04_wmi_bs=::src/lwl_nb04
		lwl_nb04_wmi_ab=::src/lwl_nb04
		lwl_nb04_power_profiles=::src/lwl_nb04
		lwl_nb04_kbd_backlight=::src/lwl_nb04
		lwl_nb04_sensors=::src/lwl_nb04
		lwl_nb05_keyboard=::src/lwl_nb05
		lwl_nb05_kbd_backlight=::src/lwl_nb05
		lwl_nb05_power_profiles=::src/lwl_nb05
		lwl_nb05_ec=::src/lwl_nb05
		lwl_nb05_sensors=::src/lwl_nb05
		lwl_nb05_fan_control=::src/lwl_nb05
		clevo_acpi=::src
		uniwill_wmi=::src
		lwl_keyboard=::src
		clevo_wmi=::src
	)
	linux-mod-r1_src_compile
}
