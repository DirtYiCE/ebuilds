# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7
inherit java-pkg-2 desktop estack xdg-utils

MY_PN=LanguageTool
MY_P=${MY_PN}-${PV}

DESCRIPTION="A proof-reading tool for many languages"
HOMEPAGE="https://www.languagetool.org/"
SRC_URI="https://www.languagetool.org/download/${MY_P}.zip"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~amd64 ~x86"

LANGUAGES=(
	ar:Arabic
	ast:Asturian
	be:Belarusian
	br:Breton
	ca:Catalan
	da:Danish
	de:German
	el:Greek
	en:English
	eo:Esperanto
	es:Spanish
	fa:Persian
	fr:French
	it:Italian
	ja:Japanese
	km:Khmer
	nl:Dutch
	pl:Polish
	pt:Portuguese
	ro:Romanian
	ru:Russian
	sk:Slovak
	sl:Slovenian
	sv:Swedish
	ta:Tamil
	tl:Tagalog
	uk:Ukrainian
	zh:Chinese
	# no maven module for them
	ga:Irish
	gl:Galician
	# ???
	tt:CrimeanTatar
)

for lang in "${LANGUAGES[@]}"; do
	IUSE+=" l10n_${lang%:*}"
done

CP_DEPEND="
	dev-java/commons-cli:1
	dev-java/commons-codec:0
	dev-java/commons-collections:4
	dev-java/commons-digester:3.2
	dev-java/commons-io:1
	dev-java/commons-lang:3.6
	dev-java/commons-logging:0
	dev-java/commons-validator:0
	dev-java/jcommander:0
	dev-java/jna:4
	dev-java/slf4j-api:0
"

RDEPEND="
	>=virtual/jre-1.8
	${CP_DEPEND}"

BDEPEND="app-arch/unzip"

S=${WORKDIR}/${MY_P}

rm_lang() {
	local id="${1%:*}"
	local name="${1#*:}"
	eshopts_push -s globstar
	rm -rf org/languagetool/{MessagesBundle_$id*.properties,**/$id} || die
	rm -f org/languagetool/**/*$name*.class || die
	sed -i "/$name/ d" META-INF/org/languagetool/language-module.properties || die

	# this enterprise piece of bloat shit fails (but only the server) if
	# you remove es/ca/pt. But you can't just keep it in the properties,
	# you need the class files, and the class files need even more
	# classfiles and they load shit in static constructor and whatever
	# else bullshit you can throw at it, so just reconstruct enough of
	# the class files that are needed for the server to start. Fucking
	# hack, but this crap is fucking shit. As a negative consequence,
	# you'll have dummy entries in `languagetool --list` output, but
	# it's still better than installing hundreds of megabytes of crap.
	if [[ $id == es || $id == ca || $id == pt ]]; then
		local dir=org/languagetool/language
		cat <<EOF > $dir/$id.java
package org.languagetool.language;
import org.languagetool.*;
import org.languagetool.rules.*;
import java.io.IOException;
import java.util.*;
public class $id extends Language {
  public String getName() { return "(Unavailable)"; }
  public String[] getCountries() { return new String[] {}; }
  public String getShortCode() { return "$id"; }
  public Contributor[] getMaintainers() { return null; }
  public List<Rule> getRelevantRules(ResourceBundle messages, UserConfig userConfig, Language motherTongue, List<Language> altLanguages) throws IOException { return null; }
  public String getCommonWordsPath() { return null; }
}
EOF
		javac -cp .:languagetool.jar $dir/$id.java || die
		rm $dir/$id.java || die

		echo "languageClasses=org.languagetool.language.$id" \
			 >> META-INF/org/languagetool/language-module.properties
	fi
	eshopts_pop
}

src_prepare() {
	default
	rm libs/commons-{cli,codec,collections4,digester,io,lang3,logging,validator}.jar || die
	rm libs/{jcommander,slf4j-api}.jar || die
	rm -r META-INF/maven || die

	# Loads of bundled stuff :(
	#java-pkg-2_src_prepare
}

src_compile() {
	local lang
	for lang in "${LANGUAGES[@]}"; do
		use l10n_${lang%:*} || rm_lang "$lang"
	done
	use l10n_de || rm_lang de-DE-x-simple-language:SimpleGerman

	# special lib files
	use l10n_ca || rm libs/catalan-pos-dict.jar || die
	use l10n_fr || rm libs/french-pos-dict.jar || die
	use l10n_de || rm libs/german-pos-dict.jar || die
	use l10n_ga || rm libs/languagetool-ga-dicts.jar || die
	use l10n_uk || rm libs/morfologik-ukrainian-lt.jar || die
	use l10n_el || rm libs/morphology-el.jar || die
	use l10n_pt || rm libs/portuguese-pos-dict.jar || die
	use l10n_es || rm libs/spanish-pos-dict.jar || die
}

src_install() {
	java-pkg_dojar *.jar libs/*.jar

	local DIR=/usr/share/${PN}/lib/language-modules
	java-pkg_addcp "${EPREFIX}${DIR}"
	insinto ${DIR}
	doins -r org META-INF

	java-pkg_dolauncher ${PN} --main org.${PN}.commandline.Main
	java-pkg_dolauncher ${PN}-gui --main org.${PN}.gui.Main
	java-pkg_dolauncher ${PN}-server --main org.${PN}.server.HTTPServer  --config /etc/languagetool.cfg
	newicon -s scalable "${FILESDIR}"/logo.svg ${PN}.svg
	domenu "${FILESDIR}"/${PN}.desktop
	newinitd "${FILESDIR}/languagetool.initd" languagetool

	dodoc CHANGES.md README.md

	unset MY_DEPEND
	java-pkg_gen-cp MY_DEPEND
	java-pkg_register-dependency "${MY_DEPEND}"
}

pkg_postinst() {
	xdg_icon_cache_update
}

pkg_postrm() {
	xdg_icon_cache_update
}
