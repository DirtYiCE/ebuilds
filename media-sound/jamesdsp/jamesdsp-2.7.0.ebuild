# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit desktop qmake-utils flag-o-matic

eeleditor_revision=b2f392480e00ca232c397610f42688b165b87640
flattabwidget_revision=06509713d85dc336c4a3b089ef9d265003aaf48e
graphiceqwidget_revision=ba63ad32682b20e2d4fde4c8a4aafe4da3423cc5
liquidequalizerwidget_revision=82a0c5240093e58b97ccbeb3716ee64e71a68d7c

SRC_URI="
	https://github.com/Audio4Linux/JDSP4Linux/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz
	https://github.com/timschneeb/LiveprogIDE/archive/${eeleditor_revision}.tar.gz -> LiveprogIDE-${eeleditor_revision}.tar.gz
	https://github.com/timschneeb/FlatTabWidget/archive/${flattabwidget_revision}.tar.gz -> FlatTabWidget-${flattabwidget_revision}.tar.gz
	https://github.com/timschneeb/GraphicEQWidget/archive/${graphiceqwidget_revision}.tar.gz -> GraphicEQWidget-${graphiceqwidget_revision}.tar.gz
	https://github.com/timschneeb/LiquidEqualizerWidget/archive/${liquidequalizerwidget_revision}.tar.gz -> LiquidEqualizerWidget-${liquidequalizerwidget_revision}.tar.gz
"
DESCRIPTION="Open-source sound effects for PipeWire and PulseAudio"
HOMEPAGE="https://github.com/Audio4Linux/JDSP4Linux"
LICENSE="GPL-3"
KEYWORDS="~amd64"
SLOT="0"
IUSE="pipewire pulseaudio"
REQUIRED_USE="^^ ( pipewire pulseaudio )"

DEPEND="
	pipewire? ( >=media-video/pipewire-0.3 )
	!pipewire? ( pulseaudio? (
		media-sound/pulseaudio
		media-libs/gstreamer
		media-libs/gst-plugins-base
	) )
	app-arch/libarchive
	>=dev-cpp/glibmm-2.4:2
	>=dev-libs/glib-2.0
	dev-qt/qtbase:6[dbus,gui,network,xml]
	dev-qt/qtsvg:6
"

RDEPEND="
	${DEPEND}
	!pipewire? ( pulseaudio? ( media-libs/gst-plugins-good ) )
"

BDEPEND="virtual/pkgconfig"

S="${WORKDIR}/JDSP4Linux-${PV}"

src_prepare() {
	default

	rmdir src/subprojects/{EELEditor,FlatTabWidget,GraphicEQWidget,LiquidEqualizerWidget} || die
	mv ../LiveprogIDE-${eeleditor_revision} src/subprojects/EELEditor || die
	mv ../FlatTabWidget-${flattabwidget_revision} src/subprojects/FlatTabWidget || die
	mv ../GraphicEQWidget-${graphiceqwidget_revision} src/subprojects/GraphicEQWidget || die
	mv ../LiquidEqualizerWidget-${liquidequalizerwidget_revision} src/subprojects/LiquidEqualizerWidget || die
}

src_configure() {
	append-cflags -Wno-incompatible-pointer-types
	append-cflags -Wno-implicit-int
	append-cflags -Wno-implicit-function-declaration

	local eqmakeargs=(
		CONFIG$(usex pipewire - $(usex pulseaudio + -))=USE_PULSEAUDIO
		DEFINES$(usex elibc_musl + -)=NO_CRASH_HANDLER
	)

	eqmake6 JDSP4Linux.pro "${eqmakeargs[@]}"
}

src_install() {
	dobin src/jamesdsp
	doicon resources/icons/icon.svg
	make_desktop_entry \
		"/usr/bin/jamesdsp" \
		"JamesDSP" \
		"jamesdsp" \
		"AudioVideo;Audio;" \
		"GenericName=Audio effect processor\nKeywords=equalizer;audio;effect\nStartupNotify=false\nTerminal=false"
}
