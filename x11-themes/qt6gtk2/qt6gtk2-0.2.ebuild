# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit qmake-utils

DESCRIPTION="GTK+2.0 integration plugins for Qt6"
HOMEPAGE="https://github.com/trialuser02/qt6gtk2"
SRC_URI="https://github.com/trialuser02/qt6gtk2/releases/download/${PV}/${P}.tar.xz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""

DEPEND="
	dev-qt/qtbase:6
	x11-libs/gtk+:2
"

src_configure() {
	eqmake6
}

src_install() {
	emake INSTALL_ROOT="${D}" install
}
