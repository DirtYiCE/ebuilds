# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
inherit linux-mod

DESCRIPTION="Realtek 8188FU driver module for Linux kernel"
HOMEPAGE="https://github.com/kelebek333/${PN}"

COMMIT="751882b3d8925b72ed796f40e38c0232ccc24785"
SRC_URI="https://github.com/kelebek333/${PN}/archive/${COMMIT}.tar.gz -> ${PN}-${COMMIT}.tar.gz"

LICENSE="GPL-2"
KEYWORDS="~amd64 ~x86"

S="${WORKDIR}/${PN}-${COMMIT}"

MODULE_NAMES="rtl8188fu(net/wireless)"
BUILD_TARGETS="all"

src_compile(){
        BUILD_PARAMS="KSRC=${KERNEL_DIR}"
        linux-mod_src_compile
}
