EAPI=8

inherit unpacker

# GENVENDOR=y

DESCRIPTION="Automatically populate the entities-list of lovelace cards"
HOMEPAGE="https://github.com/thomasloven/${PN}"
SRC_URI="https://github.com/thomasloven/${PN}/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz"
[[ -n "${GENVENDOR}" ]] || SRC_URI+=" https://home.dirty-ice.org/packages/${PN}-vendor-${PV}.tar.zst"

LICENSE="MIT"
SLOT="0"
KEYWORDS="*"
IUSE=""

DEPEND="
	acct-group/homeassistant
	acct-user/homeassistant
"
BDEPEND="
	app-arch/zstd
	net-libs/nodejs[npm]
"

PATCHES=("${FILESDIR}"/dynamic-option-values.patch)

[[ -n "${GENVENDOR}" ]] && RESTRICT="network-sandbox"

src_compile() {
	if [[ -n "${GENVENDOR}" ]]; then
		npm install || die
		einfo "Generating vendor pkg"
		file="${PN}-vendor-${PV}.tar.zst"
		tar c node_modules | zstd -v -T0 -19 > "${file}"
		zstd -l "${file}"
		die "vendor pkg created in ${S}/${file}"
	fi

	npm run build || die "build failed"
}

src_install() {
	dodoc README.md

	insinto "/etc/homeassistant/www/${PN}"
	doins auto-entities.js

	fowners -R homeassistant:homeassistant /etc/homeassistant
}
