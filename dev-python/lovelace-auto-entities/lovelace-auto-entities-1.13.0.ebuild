EAPI=8

inherit unpacker

# GENVENDOR=y

DESCRIPTION="Automatically populate the entities-list of lovelace cards"
HOMEPAGE="https://github.com/thomasloven/${PN}"
SRC_URI="https://github.com/thomasloven/${PN}/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"
[[ -n "${GENVENDOR}" ]] || SRC_URI+=" https://home.dirty-ice.org/packages/${PN}-vendor-${PV}.tar.zst"

LICENSE="MIT"
SLOT="0"
KEYWORDS="*"
IUSE=""

DEPEND="
	acct-group/homeassistant
	acct-user/homeassistant
"
BDEPEND="
	app-arch/zstd
	net-libs/nodejs[npm]
"

PATCHES=("${FILESDIR}"/dynamic-option-values.patch)

[[ -n "${GENVENDOR}" ]] && RESTRICT="network-sandbox"

src_compile() {
	if [[ -n "${GENVENDOR}" ]]; then
		npm install || die
		einfo "Generating vendor pkg"
		file_tar="${PN}-vendor-${PV}.tar"
		tar cf "${file_tar}" node_modules || die
		zstd --ultra -v -T0 -22 "${file_tar}" || die
		zstd -l "${file_tar}.zst"
		die "vendor pkg created in ${S}/${file_tar}.zst"
	fi

	npm run build || die "build failed"
}

src_install() {
	dodoc README.md

	insinto "/etc/homeassistant/www/${PN}"
	doins auto-entities.js

	fowners -R homeassistant:homeassistant /etc/homeassistant
}
