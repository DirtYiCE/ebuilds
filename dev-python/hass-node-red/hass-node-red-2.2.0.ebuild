EAPI=8

DESCRIPTION="Companion Component for node-red-contrib-home-assistant-websocket to help integrate Node-RED with Home Assistant Core"
HOMEPAGE="https://github.com/zachowj/${PN}"
SRC_URI="https://github.com/zachowj/${PN}/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE=""
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""

DEPEND="
	acct-group/homeassistant
	acct-user/homeassistant
"

src_install() {
	insinto /etc/homeassistant/custom_components
	doins -r custom_components/nodered

	fowners -R homeassistant:homeassistant /etc/homeassistant
}
