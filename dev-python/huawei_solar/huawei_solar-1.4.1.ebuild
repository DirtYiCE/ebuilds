EAPI=8

DESCRIPTION="Home Assistant integration for Huawei Solar inverters via Modbus"
HOMEPAGE="https://github.com/wlcrs/${PN}"
SRC_URI="https://github.com/wlcrs/${PN}/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE=""
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""

DEPEND="
	acct-group/homeassistant
	acct-user/homeassistant
"

src_install() {
	insinto /etc/homeassistant/custom_components/huawei_solar
	doins -r *

	fowners -R homeassistant:homeassistant /etc/homeassistant
}
