EAPI=8

DESCRIPTION="Caule Themes Pack 1 - by caule.studio"
HOMEPAGE="https://github.com/orickcorreia/caule-themes-pack-1"
SRC_URI="https://github.com/orickcorreia/caule-themes-pack-1/archive/refs/tags/v.${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""

DEPEND="
	acct-group/homeassistant
	acct-user/homeassistant
	dev-lang/ruby
"

S="${WORKDIR}/caule-themes-pack-1-v.${PV}"

src_compile() {
	#unzip src/backgrounds-icons.zip || die
	# the manual version is outdated
	ruby <<EOS || die
require 'yaml'
fname = 'themes/caule-themes-pack-1.yaml'
y = YAML.load_file fname
# fix paths
y.each {|k,t| t.each {|k2,v| v.gsub! %r{/hacsfiles/themes}, '/local' if v.start_with? 'url(' } }
# make dark/light variants
defaults = {
  "app-header-text-color" => "var(--text-primary-color)",
  "app-header-background-color" => "var(--primary-color)",
  "switch-unchecked-button-color" => "var(--switch-unchecked-color, var(--primary-background-color))",
  "switch-unchecked-track-color" => "var(--switch-unchecked-color, #000000)"
}
y = y.select {|k,v| k !~ /Light/ }.map do |k,v|
  dark = v.dup
  defaults.each {|k,d| dark[k] ||= d }
  light = y[k.gsub(/(Black|Dark)/, 'Light').gsub(' Glass', '')].dup
  common = {'modes' => { 'dark' => dark, 'light' => light } }
  dark.keys.each do |k|
    next unless dark[k] == light[k]
    common[k] = dark[k]
    dark.delete k
    light.delete k
  end
  [k, common]
end.to_h
File.open(fname, 'w') {|f| YAML.dump y, f }
EOS
}

src_install() {
	insinto /etc/homeassistant/themes
	doins themes/caule-themes-pack-1.yaml

	insinto /etc/homeassistant/www/caule-themes-pack-1
	doins themes/*.jpg
	doins themes/*.svg

	fowners -R homeassistant:homeassistant /etc/homeassistant
}
