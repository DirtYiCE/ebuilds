# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Watch a file or folder and automatically commit changes to a git repo easily"
HOMEPAGE="https://github.com/gitwatch/gitwatch"
SRC_URI="https://github.com/gitwatch/gitwatch/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="
	app-shells/bash
	dev-vcs/git
	sys-fs/inotify-tools
"

src_install() {
	default
	dobin gitwatch.sh
}
