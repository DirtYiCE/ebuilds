EAPI=8

inherit estack unpacker

# GENVENDOR=y

DESCRIPTION="Zwave to Mqtt gateway and Control Panel Web UI"
HOMEPAGE="https://zwave-js.github.io/zwavejs2mqtt/"
SRC_URI="https://github.com/zwave-js/zwavejs2mqtt/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"
[[ -n "${GENVENDOR}" ]] || SRC_URI+=" https://home.dirty-ice.org/packages/${PN}-vendor-${PV}.tar.zst"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""

DEPEND="
	acct-group/zwavejs2mqtt
	acct-user/zwavejs2mqtt
"
RDEPEND="
	net-libs/nodejs:=[icu,npm]
	!app-misc/zwavejs2mqtt-bin
"
BDEPEND="
	net-libs/nodejs:=[icu,npm]
"

[[ -n "${GENVENDOR}" ]] && RESTRICT="network-sandbox"

PATCHES="
	${FILESDIR}/${P}-fix-cwd.patch
	${FILESDIR}/zwavejs2mqtt-8.2.1-philio.patch
"

S="${WORKDIR}/zwave-js-ui-${PV}"

src_prepare() {
	if ! [[ -z "${GENVENDOR}" ]]; then
		npm install --build-from-source || die

		einfo "Generating vendor pkg"
		cd "${WORKDIR}"
		file_tar=${PN}-vendor-${PV}.tar
		tar cf ${file_tar} "zwave-js-ui-${PV}/node_modules"
		zstd --ultra -v -T0 -22 ${file_tar} || die
		zstd -l ${file_tar}.zst
		die "vendor pkg created in ${WORKDIR}/${file_tar}.zst"
	fi

	default
}

src_compile() {
	npm run build || die

	eshopts_push -s globstar
	rm -fR node_modules/**/{prebuilds,obj.target} || die
	eshopts_pop
	find . -type d -empty -delete || die
	find . -type f '(' -name '*.ts*' -o -name '*.map' -o -iname '*.md' ')' -delete || die
}

src_install() {
	insinto /usr/share/zwavejs2mqtt
	doins package.json
	doins -r server
	doins -r dist
	doins -r snippets
	doins -r node_modules

	newbin "${FILESDIR}/zwavejs2mqtt.sh" zwavejs2mqtt

	newinitd "${FILESDIR}/init" zwavejs2mqtt
	keepdir /var/lib/zwavejs2mqtt
	fowners -R zwavejs2mqtt:zwavejs2mqtt /var/lib/zwavejs2mqtt
	keepdir /var/log/zwavejs2mqtt
	fowners -R zwavejs2mqtt:zwavejs2mqtt /var/log/zwavejs2mqtt
}
