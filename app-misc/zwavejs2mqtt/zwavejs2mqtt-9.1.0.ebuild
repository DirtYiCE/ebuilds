EAPI=8

inherit estack unpacker

# GENVENDOR=y

DESCRIPTION="Zwave to Mqtt gateway and Control Panel Web UI"
HOMEPAGE="https://zwave-js.github.io/zwavejs2mqtt/"
SRC_URI="https://github.com/zwave-js/zwavejs2mqtt/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"
[[ -n "${GENVENDOR}" ]] || SRC_URI+=" https://home.dirty-ice.org/packages/${PN}-vendor-${PV}.tar.zst"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""

DEPEND="
	acct-group/zwavejs2mqtt
	acct-user/zwavejs2mqtt
"
RDEPEND="
	net-libs/nodejs:=[icu]
	!app-misc/zwavejs2mqtt-bin
"
BDEPEND="
	net-libs/nodejs:=[icu]
	sys-apps/yarn
"

[[ -n "${GENVENDOR}" ]] && RESTRICT="network-sandbox"

PATCHES="
	${FILESDIR}/fix-cwd.patch
	${FILESDIR}/zwavejs2mqtt-8.2.1-philio.patch
"

S="${WORKDIR}/zwave-js-ui-${PV}"

src_prepare() {
	# braindead node-gyp can't find node headers on it's own
	#gyp_dir=~/.cache/node-gyp/"$(node --version | sed 's/^v//')"
	#mkdir -p "${gyp_dir}/include"
	#ln -s /usr/include/node "${gyp_dir}/include/node"
	#echo 9 > "${gyp_dir}/installVersion"

	# braindead node-gyp really needs a tarball, old symlink method no longer works
	pushd /usr/include
	tar -cf "${WORKDIR}/node.tar" node --transform="s|^|node-$(node --version)/include/|"
	popd
	export npm_config_tarball="${WORKDIR}/node.tar"

	# do not cache to $HOME
	sed -i '/enableGlobalCache/d' .yarnrc.yml || die
	# npm config set nodedir /
	export npm_config_build_from_source=true
	# braindead yarn2+ doesn't have --offline, so we'll have to trust
	# the sandbox
	yarn install || die

	if ! [[ -z "${GENVENDOR}" ]]; then
		cd "${WORKDIR}"
		einfo "Generating vendor pkg"
		bytes=$(tar c zwave-js-ui-${PV}/.yarn/cache | wc -c)
		file=${PN}-vendor-${PV}.tar.zst
		tar c zwave-js-ui-${PV}/.yarn/cache | zstd -v -T0 -19 --stream-size=${bytes} > ${file} || die
		zstd -l ${file}
		die "vendor pkg created in ${WORKDIR}/${file}"
	fi

	default
}

src_compile() {
	yarn run build || die

	eshopts_push -s globstar
	rm -fR node_modules/**/{prebuilds,obj.target} || die
	eshopts_pop
	find . -type d -empty -delete || die
	find . -type f '(' -name '*.ts*' -o -name '*.map' -o -iname '*.md' ')' -delete || die
}

src_install() {
	insinto /usr/share/zwavejs2mqtt
	doins -r server
	doins -r dist
	doins -r snippets
	doins -r node_modules

	newbin "${FILESDIR}/zwavejs2mqtt.sh" zwavejs2mqtt

	newinitd "${FILESDIR}/init" zwavejs2mqtt
	keepdir /var/lib/zwavejs2mqtt
	fowners -R zwavejs2mqtt:zwavejs2mqtt /var/lib/zwavejs2mqtt
	keepdir /var/log/zwavejs2mqtt
	fowners -R zwavejs2mqtt:zwavejs2mqtt /var/log/zwavejs2mqtt
}
