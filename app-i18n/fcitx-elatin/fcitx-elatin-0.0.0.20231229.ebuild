# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

MY_COMMIT=0cf0f8f496e64ce6208da4577fb04955e82eb652

inherit cmake

DESCRIPTION="Emacs-like latin input method for fcitx5"
HOMEPAGE="https://codeberg.org/DirtYiCE/fcitx-elatin"
SRC_URI="https://codeberg.org/DirtYiCE/${PN}/archive/${MY_COMMIT}.tar.gz -> ${P}.tar.gz"

LICENSE="WTFPL2"
SLOT="5"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND=">=app-i18n/fcitx-5.1.5:5"
DEPEND="${RDEPEND}"
BDEPEND=""

S="${WORKDIR}/${PN}"
