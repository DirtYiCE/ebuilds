# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
inherit linux-mod-r1

DESCRIPTION="yt6801 kernel driver"
HOMEPAGE="https://www.motor-comm.com/product/ethernet-control-chip"

MY_PV="${PV}tux0"
SRC_URI="https://www.motor-comm.com/Public/Uploads/uploadfile/files/20240812/yt6801-linux-driver-${PV}.zip"

LICENSE="GPL-2"
KEYWORDS="~amd64 ~x86"
SLOT="0"

S="${WORKDIR}"

# see info here: https://github.com/silent-reader-cn/yt6801
src_compile(){
	local modlist=(yt6801=kernel/drivers/net/ethernet/motorcomm::src)
	linux-mod-r1_src_compile
}
